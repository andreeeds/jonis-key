from os import path

__all__ = [ 'Maze' ]

class Maze(object):
  '''
  Maze detail information

  Attributes:
    __maze: list - maze container in list of string
    __width: int - width of maze
    __height: int - height of maze
    __X: tuple - start position ( x, y )
  '''
  def __init__(self):
    self.__maze = []
    self.__width = 0
    self.__height = 0
    self.__X = ( 0, 0 )

    # load default maze
    filePath = 'default.txt'
    if path.exists(filePath):
      with open(filePath, 'r') as f:
        lines = [ line.rstrip() for line in f ]
        self.newMaze(lines)

  def newMaze(self, maze):
    '''
    Check validity of new maze, each row has same size
    Check start location

    Args:
      maze: list - new maze in list of string

    Return:
      boolean: True if new maze is valid
    '''
    if len(maze) == 0:
      # new maze is empty list
      return False

    width = None
    for i, m in enumerate(maze):
      if width is None:
        # initialize width size
        width = len(m)
      else:
        if width != len(m):
          # compare width size for each row
          self.__X = ( 0, 0 )
          return False

      if 'X' in m:
        # get start position
        self.__X = (m.index('X'), i)

    if self.__X == ( 0, 0 ):
      # new maze doesn't have start position
      return False

    # update maze information
    self.__maze = maze.copy()
    self.__width = width
    self.__height = len(maze)

    return True

  def getMaze(self):
    return self.__maze

  def checkPosibility(self):
    '''
    Calculate total posibility location of key

    Args:
      None

    Return:
      int: total posibility location
    '''
    # check step for north rule
    return self.__ruleNorth(self.__X[0], self.__X[1] - 1)

  def __baseZeroRule(self, x, y):
    '''
    Base for not posible location
    '''
    if x < 0 or x >= self.__width or y < 0 or y >= self.__height:
      # index out of bound
      return True

    if self.__maze[y][x] == '#':
      # unavailable floor
      return True

    return False

  def __ruleNorth(self, x, y):
    '''
    Check for the north step
    '''
    if self.__baseZeroRule(x,y):
      return 0

    if self.__maze[y][x] == '.':
      # available step
      # check posibility step for keep in north rule and posibility step
      # for east rule
      return self.__ruleNorth(x, y - 1) + self.__ruleEast(x + 1, y)

    return 0

  def __ruleEast(self, x, y):
    '''
    Check for the east step
    '''
    if self.__baseZeroRule(x,y):
      return 0

    if self.__maze[y][x] == '.':
      # available step
      # check posibility step for keep in east rule and posibility step
      # for south rule
      return self.__ruleEast(x + 1, y) + self.__ruleSouth(x, y + 1)

    return 0

  def __ruleSouth(self, x, y):
    '''
    Check for the south step
    '''
    if self.__baseZeroRule(x,y):
      return 0

    if self.__maze[y][x] == '.':
      # got one posibility
      # check posibility step for keep in south rule
      return 1 + self.__ruleSouth(x, y + 1)

    return 0

if __name__ == '__main__':
  maze = Maze()
  print(maze.getMaze())
  print(maze.checkPosibility())
